package com.oneToMany.Entity;

import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private int cid;
	private String name ;
	private String email;
	private String gender;
	
	@OneToMany(targetEntity = Products.class , cascade = CascadeType.ALL )
	@JoinColumn( name = "fk_cp_id" , referencedColumnName = "cid")
	private List<Products> products;	
}
