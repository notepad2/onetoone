package com.oneToMany.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Products {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private int pid;
	private String pname;
	private Integer price;
	
}
