package com.oneToMany.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oneToMany.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {

	List<Customer> findByName(String name);

	List<Customer> findByGender(String gender);

	Customer findByEmail(String mail);

}
