package com.oneToMany.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oneToMany.Entity.Products;

@Repository
public interface ProductRepository extends JpaRepository<Products, Integer> {

//	List<Products> findByPriceGreaterThan(Integer price);

 	List<Products> findByPname(String pname);

	List<Products> findByPriceLessThan(Integer price);

//	List<Products> findByPrice(Integer price);

}
