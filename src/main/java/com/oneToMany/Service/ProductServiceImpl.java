package com.oneToMany.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oneToMany.CustomerDto.ProductDto;
import com.oneToMany.Entity.Products;
import com.oneToMany.Repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService   {

	
	@Autowired
	private ProductRepository productRepository;
	



	@Override
	public List<Products> findByPname(String pname) {
		
		List<Products> list = productRepository.findByPname(pname);
		return list;
	}

	@Override
	public List<ProductDto> findByPriceGreaterThan(Integer price) {
		List<Products> products = productRepository.findByPriceLessThan(price);
		
		List<ProductDto> list = new ArrayList<>();
		
		for ( Products products2 : products) {
			ProductDto dto = new ProductDto();
			dto.setPid(products2.getPid());
			dto.setPname(products2.getPname());
			dto.setPrice(products2.getPrice());
			list.add(dto);
		}
		return list;		
	}
}
