package com.oneToMany.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.oneToMany.CustomerDto.CustomerDto;
import com.oneToMany.CustomerDto.ProductDto;
import com.oneToMany.Entity.Customer;
import com.oneToMany.Entity.Products;

import com.oneToMany.Repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	Convert convert = new Convert();

	@Override
	public CustomerDto saveCustomers(CustomerDto customerDto) {
		Customer customer = new Customer();

		customer.setCid(customerDto.getCid());
		customer.setName(customerDto.getName());
		customer.setEmail(customerDto.getEmail());
		customer.setGender(customerDto.getGender());
		customer.setProducts(convert.convertToProduct(customerDto.getProductDtos()));
		;
		Customer customer2 = customerRepository.save(customer);

		CustomerDto customerDto2 = new CustomerDto();

		customerDto2.setCid(customer2.getCid());
		customerDto2.setName(customer2.getName());
		customerDto2.setEmail(customer2.getEmail());
		customerDto2.setGender(customer2.getGender());
		customerDto2.setProductDtos(convert.convertToProductDto(customer2.getProducts()));

		return customerDto2;

	}

	@Override
	public List<CustomerDto> findAllCustomers() {

		List<Customer> customers = customerRepository.findAll();

		List<CustomerDto> customerDtos = new ArrayList<>();

		for (Customer customer : customers) {

			CustomerDto customerDto = new CustomerDto();

			customerDto.setCid(customer.getCid());
			customerDto.setName(customer.getName());
			customerDto.setEmail(customer.getEmail());
			customerDto.setGender(customer.getGender());
			customerDto.setProductDtos(convert.convertToProductDto(customer.getProducts()));

			customerDtos.add(customerDto);
		}
		return customerDtos;
	}

	@Override // GetCustomersByName
	public List<CustomerDto> getByName(String name) {
		List<Customer> customers = customerRepository.findByName(name);
		List<CustomerDto> list = new ArrayList<>();
		for (Customer customer : customers) {
			CustomerDto customerDto = new CustomerDto();

			customerDto.setCid(customer.getCid());
			customerDto.setName(customer.getName());
			customerDto.setEmail(customer.getEmail());
			customerDto.setGender(customer.getGender());
			customerDto.setProductDtos(convert.convertToProductDto(customer.getProducts()));

			list.add(customerDto);
		}
		return list;
	}

	@Override
	public List<CustomerDto> findByGender(String gender) {
		List<Customer> customers = customerRepository.findByGender(gender);
		List<CustomerDto> list = new ArrayList<>();
		for (Customer customer : customers) {

			CustomerDto customerDto = new CustomerDto();

			customerDto.setCid(customer.getCid());
			customerDto.setName(customer.getName());
			customerDto.setEmail(customer.getEmail());
			customerDto.setGender(customer.getGender());
			customerDto.setProductDtos(convert.convertToProductDto(customer.getProducts()));

			list.add(customerDto);
		}
		return list;
	}

	@Override
	public CustomerDto getByEmail(String mail) {
			Customer customer	= customerRepository.findByEmail(mail);
			CustomerDto dto = convert.changeToCustomerDto(customer);
			return dto;
	}
}

@Component
class Convert {
	public List<Products> convertToProduct(List<ProductDto> dtos) {

		List<Products> list = new ArrayList<>();

		for (ProductDto productDto : dtos) {
			Products products = new Products();
			products.setPid(productDto.getPid());
			products.setPname(productDto.getPname());
			products.setPrice(productDto.getPrice());

			list.add(products);
		}
		return list;

	}

	public List<ProductDto> convertToProductDto(List<Products> products) {

		List<ProductDto> list = new ArrayList<>();
		for (Products products2 : products) {

			ProductDto dto = new ProductDto();

			dto.setPid(products2.getPid());
			dto.setPname(products2.getPname());
			dto.setPrice(products2.getPrice());
			list.add(dto);
		}
		return list;
	}
	
	public CustomerDto changeToCustomerDto(Customer customer ) {
		
		
		CustomerDto customerDto = new CustomerDto();
		
		customerDto.setCid(customer.getCid());
		customerDto.setName(customer.getName() );
		customerDto.setEmail(customer.getEmail());
		customerDto.setGender(customer.getGender());
		customerDto.setProductDtos(convertToProductDto(customer.getProducts()));
		
		return customerDto;
	}
}
