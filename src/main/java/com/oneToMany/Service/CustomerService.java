package com.oneToMany.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.oneToMany.CustomerDto.CustomerDto;

@Service
public interface CustomerService {


	CustomerDto saveCustomers(CustomerDto customerDto);

	List<CustomerDto> findAllCustomers();

	List<CustomerDto> getByName(String name);

	List<CustomerDto> findByGender(String gender);

	CustomerDto getByEmail(String mail);

}
