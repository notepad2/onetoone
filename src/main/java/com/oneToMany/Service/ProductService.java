package com.oneToMany.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.oneToMany.CustomerDto.ProductDto;
import com.oneToMany.Entity.Products;

@Service
public interface ProductService  {

//	List<Products> getPrice(Integer price);


	List<Products> findByPname(String pname);

	List<ProductDto> findByPriceGreaterThan(Integer price);

}
