package com.oneToMany.CustomerDto;

import java.util.List;

import com.oneToMany.Entity.Customer;
import com.oneToMany.Entity.Products;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class CustomerDto {

	
	private int cid;
	private String name ;
	private String email;
	private String gender;
	private List<ProductDto> productDtos;
	

}
