package com.oneToMany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oneToMany.CustomerDto.CustomerDto;
import com.oneToMany.CustomerDto.ProductDto;
import com.oneToMany.Entity.Products;
import com.oneToMany.Service.CustomerService;
import com.oneToMany.Service.ProductService;

@RestController
@RequestMapping("/customer/product")
public class Controller {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private ProductService productService;
	
	@PostMapping("/saveCustomers")
	public CustomerDto saveCustomer(@RequestBody CustomerDto  customerDto) {
		CustomerDto customerDto2	=	customerService.saveCustomers(customerDto);
		return customerDto2;
	}
	
	@GetMapping("/allCustomers")
	public List<CustomerDto > allCustomers(){
	List<CustomerDto> customerDtos	=customerService.findAllCustomers();
	return customerDtos;
	}	
	
	
	@GetMapping("/getby/{name}")
	public List<CustomerDto> getByName(@PathVariable String name ){
		List<CustomerDto> customerDtos = customerService.getByName(name);
		return customerDtos;
	}
	
	@GetMapping("/getByPrice/{price}")
	public List<ProductDto> getByPrice(@PathVariable Integer price ) {
					List<ProductDto> productDtos	=productService.findByPriceGreaterThan(price );
					return productDtos;
	}
	
	@GetMapping("/getByName/{pname}")
	public List<Products> getByPname(@PathVariable String pname ) {
		List<Products> list = productService.findByPname(pname);
		return list;
	}
	
	@GetMapping("/getByGender/{gender}")
	public List<CustomerDto>  getByGender(@PathVariable String gender ) {
			List<CustomerDto> list	=	customerService.findByGender(gender);
			return list;
	}
	
	@GetMapping("/getByMail/{mail}")
	public CustomerDto getByEmail(@PathVariable String mail ) {
		return customerService.getByEmail(mail);
	}
}
